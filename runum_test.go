package carnum

import (
	"testing"
)

func TestRuNum_Parse(t *testing.T) {
	abcNum := Parse("A123BC199")
	numTable := [...]struct {
		input    string
		expected *RuNum
	}{
		{"A123BC199", abcNum},
		{"а123вс199", abcNum}, // Cyr
		{"А123ВС199", abcNum}, // Cyr
		{"A123BC_199", abcNum},
		{"A123BC 199", abcNum},
		{"A_123_BC_199", abcNum},
		{" A 123 BC 199 ", abcNum},
		{"A123BC199", abcNum},

		{"A-23BC199", nil},
		{"A123BC1", nil},
		{"А123ВВ1", nil}, // Cyr
		{"R123BC199", nil},
		{"X999XXX199", nil},
		{"X999XX-199", nil},
		{"618000131055", nil},
	}
	for _, num := range numTable {
		cn := Parse(num.input)
		if (cn != nil && num.expected != nil && *cn != *num.expected) || (cn == nil && num.expected != nil) {
			t.Errorf("carnum.Parse(%q) = %v, expected %v", num.input, cn, num.expected)
		}
	}
}

func TestRuNum_Compare(t *testing.T) {
	cn1 := Parse("A123BC_199")
	cn2 := Parse("а123вс199")

	if *cn1 != *cn2 {
		t.Errorf("'%s' not equal '%s'", cn1.String(), cn2.String())
	}

	if cn1.String() != cn2.String() {
		t.Errorf("'%s' not equal '%s'", cn1.String(), cn2.String())
	}
}

func TestRuNum_String(t *testing.T) {
	numTable := [...]struct {
		input    RuNum
		expected string
	}{
		{*Parse("A058KY799"), "A058KY799"},
		{*Parse("A000KY799"), "A000KY799"},
		{*Parse("A000KY79"), "A000KY79"},
		{*Parse("A000KУ79"), "A000KY79"},
	}
	for _, num := range numTable {
		if num.input.String() != num.expected {
			t.Errorf("carnum.RegNum.String() = %v, expected %v", num.input, num.expected)
		}
	}
}

// BenchmarkParse-4   	  600414	      2126 ns/op
// BenchmarkParse-4   	  765326	      1574 ns/op (ToUpper only symbols)
// BenchmarkParse-4   	12837972	        93.5 ns/op (Without Scanf)
// BenchmarkParse-4   	12174154	        93.8 ns/op (Use map)
func BenchmarkParse(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Parse("A123BC199")
	}
}

// BenchmarkParseCyr-4   	  510250	      2650 ns/op
// BenchmarkParseCyr-4   	 2263276	       538 ns/op (Without Scanf)
// BenchmarkParseCyr-4   	 4427271	       264 ns/op (Use map)
func BenchmarkParseCyr(b *testing.B) {
	for i := 0; i < b.N; i++ {
		_ = Parse("а123вс199")
	}
}
