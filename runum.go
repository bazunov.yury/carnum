package carnum

import (
	"fmt"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	allowSymbols = " _"
	minLength    = 8
)

var seriesAbc = map[rune]byte{
	// Lat - Lat
	'A': 'A',
	'B': 'B',
	'E': 'E',
	'K': 'K',
	'M': 'M',
	'H': 'H',
	'O': 'O',
	'P': 'P',
	'C': 'C',
	'T': 'T',
	'Y': 'Y',
	'X': 'X',
	// lat - Lat
	'a': 'A',
	'b': 'B',
	'e': 'E',
	'k': 'K',
	'm': 'M',
	'h': 'H',
	'o': 'O',
	'p': 'P',
	'c': 'C',
	't': 'T',
	'y': 'Y',
	'x': 'X',
	// Cyr - Lat
	'А': 'A',
	'В': 'B',
	'Е': 'E',
	'К': 'K',
	'М': 'M',
	'Н': 'H',
	'О': 'O',
	'Р': 'P',
	'С': 'C',
	'Т': 'T',
	'У': 'Y',
	'Х': 'X',
	// cyr - Lat
	'а': 'A',
	'в': 'B',
	'е': 'E',
	'к': 'K',
	'м': 'M',
	'н': 'H',
	'о': 'O',
	'р': 'P',
	'с': 'C',
	'т': 'T',
	'у': 'Y',
	'х': 'X',
}

type RuNum struct {
	series [3]byte
	number uint16
	region uint16
}

func Parse(text string) *RuNum {
	if utf8.RuneCountInString(text) < minLength {
		return nil
	}
	n := 1
	s := 0
	num := new(RuNum)
	for _, r := range text {
		if unicode.IsNumber(r) {
			if 2 <= n && n <= 4 {
				i := uint16(r - '0')
				num.number = num.number*10 + i
				n++
			} else if 7 <= n && n <= 9 {
				i := uint16(r - '0')
				num.region = num.region*10 + i
				n++
			} else {
				return nil
			}
		} else if unicode.IsLetter(r) {
			if n == 1 || n == 5 || n == 6 {
				if b, err := serialLetter(r); err == nil {
					num.series[s] = b
					s++
					n++
				} else {
					return nil
				}
			} else {
				return nil
			}
		} else {
			if !strings.ContainsRune(allowSymbols, r) {
				return nil
			}
		}
	}
	return num
}

func (n *RuNum) String() string {
	return fmt.Sprintf("%c%03d%c%c%d",
		n.series[0],
		n.number,
		n.series[1], n.series[2],
		n.region)
}

func serialLetter(r rune) (byte, error) {
	if b, ok := seriesAbc[r]; ok {
		return b, nil
	}
	return 0, fmt.Errorf("invalid character %c", r)
}
